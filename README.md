# kit-hpc-openvpn-client-conf

This repository contains the client configuration files for the KIT HPC-VPN service.

KIT HPC-VPN offers L3 VPN access. It is a split VPN which routes only the traffic to the HPC ressources through VPN.

* Tunnel endpoint IP protocol: **IPv4** or IPv6
* Protocol: **UDP** or TCP
* Port: **1194** or 443

There are two configuration files provided (protocol udp, port 1194). One for the IPv4 tunnel endpoint and one for the IPv6 tunnel endpoint. You can change protocol to tcp and/or port to 443.

All configuration files are maintained in this git repository. The version number can be found in the header of every configuration file.
It is composed of the server-side OpenVPN version, a hyphen and a number which is incremented on every configuration change (e.g. 2.5-1).
Even if only parts of the configuration files are changed the number is always increased in every file.

Non-default or non-obvious configuration settings are explained inside every config file.

## Hints

* OpenVPN client minimum version 2.5 is required.
* In NetworkManager on Linux you have to configure: IPv6 Settings → Routes... check "Use this connection only for resources on its network".

